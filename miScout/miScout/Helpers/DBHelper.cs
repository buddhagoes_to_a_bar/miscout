﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace miScout.Helpers
{
    public static class DbHelper
    {
        /// <summary>
        /// 
        /// </summary>
        private static string ServerConnectionString
        {
            get { return string.Format(@"Server=(localdb)\v11.0;Integrated Security=true;"); }
        }
        /// <summary>
        /// Mounts the given database in to the local db
        /// </summary>
        /// <param name="databasePath">The path to the physical mdf file</param>
        public static void MountDatabaseFromFile(string databasePath)
        {
            DbHelper.Detach("miScoutDb");
            string databaseConnectionString = string.Format(@"{0}AttachDbFileName={1};Database={2};MultipleActiveResultSets=True", ServerConnectionString, databasePath, "miScoutDb");
            using (var sqlConnection = new SqlConnection(databaseConnectionString))
            {
                sqlConnection.Open();
                

       //         sqlConnection.Close();
            }
        }
        /// <summary>
        /// Detaches an existing database from the localdb
        /// </summary>
        public static void Detach(string dbName)
        {
            using (var connection = new SqlConnection(@"Server=(localdb)\v11.0;Integrated Security=true;"))
            {
                try
                {
                    connection.Open();
                    var dbExistsCommand = string.Format("SELECT * FROM master.dbo.sysdatabases WHERE name = '{0}'", dbName);
                    using (var sqlCommand = new SqlCommand(dbExistsCommand, connection))
                    {
                        var sqlReader = sqlCommand.ExecuteReader();
                        var hasRow = sqlReader.HasRows;
                        sqlReader.Close();
                        if (!hasRow) return;
                    }
                    var detachDbCommand = string.Format("USE MASTER; ALTER DATABASE {0} SET OFFLINE WITH ROLLBACK IMMEDIATE; EXEC sp_detach_db {0};", dbName);
                    using (var sqlCommand = new SqlCommand(detachDbCommand, connection))
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public static void SetSelectedLanguages(object checkedItems)
        {
            
        }
    }
}
