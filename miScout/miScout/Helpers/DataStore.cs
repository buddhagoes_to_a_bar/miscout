﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Utils.Frames;
using miScout.TableData;

namespace miScout.Helpers
{
    public class DataStore
    {
        public string SqlConnectionString { get; set; }
        public List<Language> Languages { get; set; }

        public DataStore()
        {
            LoadLanguages();
            
        }

        private void LoadLanguages()
        {
            using (var sourceConnection = new SqlConnection(SqlConnectionString))
            {
                sourceConnection.Open();

                var myCommand = new SqlCommand("SELECT * FROM " +"dbo.Languages;",sourceConnection);

                SqlDataReader myReader = myCommand.ExecuteReader();

               
                Languages = new List<Language>();
                while (myReader.Read())
                {
                    Languages.Add(new Language
                    {
                        Id = Guid.Parse(myReader["Id"].ToString()),
                        Description = myReader["Description"].ToString()
                    });
                } 
            }
           
        }
    }
}
