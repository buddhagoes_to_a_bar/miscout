﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data.Helpers;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using miScout.Helpers;

namespace miScout
{

    public partial class ScoutForm : Form
    {
        #region _WIN32_DEFINITIONS
        private const int WM_NCHITTEST = 0x84;
        private const int HTCLIENT = 0x1;
        private const int HTCAPTION = 0x2;
        #endregion

        private Image localImage;
        private readonly string[] _pageNames =
        {
            "New Match", "Load Match", "Preferences", "Player Profile", "Player Statistics", "Upload Profile",
            "Download Profile"
        };
        protected override void WndProc(ref Message message)
        {
            base.WndProc(ref message);

            if (message.Msg == WM_NCHITTEST && (int)message.Result == HTCLIENT)
                message.Result = (IntPtr)HTCAPTION;
        }
        /// <summary>
        /// 
        /// </summary>
        public ScoutForm()
        {
            InitializeComponent();
            CheckForValidPreferences();
            DbHelper.MountDatabaseFromFile(@"C:\Prasanna\miscout\miScout\miScout\Data\miScoutDb_Primary.mdf");
            languagesTableAdapter.Fill(miScoutDbDataSet.Languages);
            //InsertClubs();
        }

        private void InsertClubs()
        {
            using (var connection = new SqlConnection(@"Server=(localdb)\v11.0;Integrated Security=true;Database=miScoutDb"))
            {
                connection.Open();
                string[] fileEntries = Directory.GetFiles(@"C:\Prasanna\miscout\Design Documents\miScout-Requirements-May17\International Badges\Europe");
                foreach (string fileName in fileEntries)
                {
                    byte[] photo = GetPhoto(fileName);
                    var file = Path.GetFileNameWithoutExtension(fileName);
                    
                    SqlCommand sqlCommand = new SqlCommand(@"INSERT INTO COUNTRIES (Id,Description,BadgeImage) VALUES(@Id,@Description,@BadgeImage)", connection);
                    sqlCommand.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Value = Guid.NewGuid();
                    sqlCommand.Parameters.Add("@Description", SqlDbType.VarChar, 100).Value = file;
                    sqlCommand.Parameters.Add("@BadgeImage", SqlDbType.Image, photo.Length).Value = photo;
                    sqlCommand.ExecuteNonQuery();
                }
               
            }
        }
        public static byte[] GetPhoto(string filePath)
        {
            FileStream stream = new FileStream(
                filePath, FileMode.Open, FileAccess.Read);
            BinaryReader reader = new BinaryReader(stream);

            byte[] photo = reader.ReadBytes((int)stream.Length);

            reader.Close();
            stream.Close();

            return photo;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            DbHelper.Detach("miScoutDb");
            Cursor = Cursors.Default;
            Close();
        }
        /// <summary>
        /// Checks if the player has filled preferences.
        ///  If not, then the user cannot select other pages
        /// until prefrences are filled out
        /// </summary>
        private void CheckForValidPreferences()
        {
            xtraTabControl.TabPages[0].PageEnabled = false;
            xtraTabControl.TabPages[1].PageEnabled = false;
            xtraTabControl.TabPages[3].PageEnabled = false;
            xtraTabControl.TabPages[4].PageEnabled = false;
            xtraTabControl.TabPages[5].PageEnabled = false;
            xtraTabControl.TabPages[6].PageEnabled = false;
            xtraTabControl.SelectedTabPageIndex = 2;
            lblHeader.Text = _pageNames[xtraTabControl.SelectedTabPageIndex];
        }
        /// <summary>
        /// Called when the user selects another page
        /// 1. Update the Header Label text indicating which page we are on
        /// </summary>
        /// <param name="sender">Sender of this message (Tab Control)</param>
        /// <param name="e">Info on Currently selected and previous pages</param>
        private void xtraTabControl_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            lblHeader.Text = _pageNames[xtraTabControl.SelectedTabPageIndex];
        }

        private void ScoutForm_Load(object sender, EventArgs e)
        {
            languagesTableAdapter.Fill(miScoutDbDataSet.Languages);
            // TODO: This line of code loads data into the 'miScoutDbDataSet.PlayerData' table. You can move, or remove it, as needed.
            playerDataTableAdapter1.Fill(miScoutDbDataSet.PlayerData);
            
            countriesTableAdapter.Fill(miScoutDbDataSet.Countries);
            // TODO: This line of code loads data into the 'miScoutDbDataSet.Clubs' table. You can move, or remove it, as needed.
            clubsTableAdapter.Fill(miScoutDbDataSet.Clubs);

            var selectedLanguages = (from language in miScoutDbDataSet.Languages where language.Selected select language.Description).ToList();
            cmbPlayerLanguages.EditValue = string.Join(",", selectedLanguages);

            var clubImage = miScoutDbDataSet.Countries.Single(a => a.Id == Guid.Parse(miScoutDbDataSet.PlayerData.Rows[0].ItemArray[4].ToString()));
            var ms = new System.IO.MemoryStream(clubImage.BadgeImage);
            localImage = Image.FromStream(ms);
            countryBadge.Image = localImage;
        }
        /// <summary>
        /// Save Settings back to db
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveSettings_Click(object sender, EventArgs e)
        {
           foreach (CheckedListBoxItem item in cmbPlayerLanguages.Properties.Items)
            {
                object item1 = item;
                var languageItem = miScoutDbDataSet.Languages.SingleOrDefault(a => a.Description == item1.ToString());
                if(languageItem == null)continue;
                languageItem.Selected = item.CheckState == CheckState.Checked;

            }
            languagesTableAdapter.Update(miScoutDbDataSet.Languages);
            Guid SelectedNationality = Guid.Empty;
            foreach (CheckedListBoxItem item in cmbNationality.Properties.Items)
            {
                object item1 = item;
                var countryItem = miScoutDbDataSet.Countries.SingleOrDefault(a => a.Description == item1.ToString());
                if (countryItem == null) continue;
                SelectedNationality = countryItem.Id;
            }
            if (miScoutDbDataSet.PlayerData.Rows.Count == 0)
            {
                 miScoutDbDataSet.PlayerData.Clear();
                 playerDataTableAdapter1.Update(miScoutDbDataSet.PlayerData);
            }
            
            miScoutDbDataSet.PlayerData.Rows.Add(Guid.NewGuid(), txtFirstName.Text, txtLastName.Text, txtPreferredName.Text, SelectedNationality, 0,
                    0, txtPassword.Text);
                playerDataTableAdapter1.Update(miScoutDbDataSet.PlayerData);
         
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScoutForm_Paint(object sender, PaintEventArgs e)
        {
            PointF point1 = PointF.Empty;
            PointF point2 = PointF.Empty;

            point1.X = 0;
            point1.Y = topPanel.Bottom + 5;
            point2.X = Width;
            point2.Y = topPanel.Bottom + 5;
            float[] dashValues = { 4, 4 };
            var dashPen = new Pen(Color.FromArgb(86,211,211), 2) {DashPattern = dashValues};
            e.Graphics.DrawLine(dashPen, point1, point2);
        }

        private void cmbNationality_EditValueChanged(object sender, EventArgs e)
        {

        }

       
    }
}
