﻿using System;

namespace miScout.TableData
{
    public class Club
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
    }
}
