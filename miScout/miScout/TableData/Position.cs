﻿using System;

namespace miScout.TableData
{
    public class Position
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
    }
}
