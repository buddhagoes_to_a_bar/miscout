﻿using System;
using System.Collections.Generic;

namespace miScout.TableData
{
    public class Player
    {
        public Guid PlayerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PreferredName { get; set; }
        public Guid? NationalityId { get; set; }
        public Guid? CurrentClubId { get; set; }
        public Guid? PositionId { get; set; }
        public int? Height { get; set; }
        public int? Weight { get; set; }
        public string Password { get; set; }
        public List<Language> PlayerLanguages { get; set; }
        public List<Position> PlayerPositions { get; set; }
        public List<Club> PreviousClubs { get; set; }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public Player()
        {
            PlayerLanguages = new List<Language>();
            PlayerPositions = new List<Position>();
            PreviousClubs = new List<Club>();
        }
    }
}
