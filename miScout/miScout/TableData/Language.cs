﻿using System;

namespace miScout.TableData
{
    public class Language
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
    }
}
