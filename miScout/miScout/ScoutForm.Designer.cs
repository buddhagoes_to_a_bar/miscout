﻿namespace miScout
{
    partial class ScoutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScoutForm));
            this.xtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.countryBadge = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.btnSaveSettings = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.checkedComboBoxEdit3 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.clubsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.miScoutDbDataSet = new miScout.miScoutDbDataSet();
            this.txtPassword = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.checkedComboBoxEdit4 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.txtWeight = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtHeight = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.checkedComboBoxEdit6 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.playerDataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.checkedComboBoxEdit5 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.cmbPlayerLanguages = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.languagesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtFirstName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtLastName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtPreferredName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.cmbNationality = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.countriesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dateOfBirth = new System.Windows.Forms.DateTimePicker();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage7 = new DevExpress.XtraTab.XtraTabPage();
            this.topPanel = new System.Windows.Forms.Panel();
            this.lblHeader = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.CloseButton = new DevExpress.XtraEditors.SimpleButton();
            this.languagesTableAdapter = new miScout.miScoutDbDataSetTableAdapters.LanguagesTableAdapter();
            this.clubsTableAdapter = new miScout.miScoutDbDataSetTableAdapters.ClubsTableAdapter();
            this.countriesTableAdapter = new miScout.miScoutDbDataSetTableAdapters.CountriesTableAdapter();
            this.playerDataTableAdapter1 = new miScout.miScoutDbDataSetTableAdapters.PlayerDataTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).BeginInit();
            this.xtraTabControl.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.countryBadge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clubsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.miScoutDbDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerDataBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPlayerLanguages.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.languagesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPreferredName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbNationality.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.countriesBindingSource)).BeginInit();
            this.topPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraTabControl
            // 
            this.xtraTabControl.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.xtraTabControl.Appearance.BackColor = System.Drawing.Color.White;
            this.xtraTabControl.Appearance.Options.UseBackColor = true;
            this.xtraTabControl.AppearancePage.Header.BackColor = System.Drawing.Color.White;
            this.xtraTabControl.AppearancePage.Header.BorderColor = System.Drawing.Color.White;
            this.xtraTabControl.AppearancePage.Header.Options.UseBackColor = true;
            this.xtraTabControl.AppearancePage.Header.Options.UseBorderColor = true;
            this.xtraTabControl.AppearancePage.HeaderActive.BackColor = System.Drawing.Color.White;
            this.xtraTabControl.AppearancePage.HeaderActive.BorderColor = System.Drawing.Color.White;
            this.xtraTabControl.AppearancePage.HeaderActive.Options.UseBackColor = true;
            this.xtraTabControl.AppearancePage.HeaderActive.Options.UseBorderColor = true;
            this.xtraTabControl.AppearancePage.HeaderHotTracked.BackColor = System.Drawing.Color.White;
            this.xtraTabControl.AppearancePage.HeaderHotTracked.BorderColor = System.Drawing.Color.White;
            this.xtraTabControl.AppearancePage.HeaderHotTracked.Options.UseBackColor = true;
            this.xtraTabControl.AppearancePage.HeaderHotTracked.Options.UseBorderColor = true;
            this.xtraTabControl.AppearancePage.PageClient.BackColor = System.Drawing.Color.White;
            this.xtraTabControl.AppearancePage.PageClient.BorderColor = System.Drawing.Color.White;
            this.xtraTabControl.AppearancePage.PageClient.Options.UseBackColor = true;
            this.xtraTabControl.AppearancePage.PageClient.Options.UseBorderColor = true;
            this.xtraTabControl.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.xtraTabControl.BorderStylePage = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.xtraTabControl.HeaderButtons = DevExpress.XtraTab.TabButtons.None;
            this.xtraTabControl.HeaderButtonsShowMode = DevExpress.XtraTab.TabButtonShowMode.WhenNeeded;
            this.xtraTabControl.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left;
            this.xtraTabControl.HeaderOrientation = DevExpress.XtraTab.TabOrientation.Horizontal;
            this.xtraTabControl.Location = new System.Drawing.Point(1, 107);
            this.xtraTabControl.LookAndFeel.SkinMaskColor = System.Drawing.Color.White;
            this.xtraTabControl.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.White;
            this.xtraTabControl.LookAndFeel.SkinName = "Seven Classic";
            this.xtraTabControl.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.xtraTabControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.xtraTabControl.MultiLine = DevExpress.Utils.DefaultBoolean.False;
            this.xtraTabControl.Name = "xtraTabControl";
            this.xtraTabControl.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl.ShowHeaderFocus = DevExpress.Utils.DefaultBoolean.False;
            this.xtraTabControl.ShowTabHeader = DevExpress.Utils.DefaultBoolean.True;
            this.xtraTabControl.Size = new System.Drawing.Size(1039, 581);
            this.xtraTabControl.TabIndex = 0;
            this.xtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4,
            this.xtraTabPage5,
            this.xtraTabPage6,
            this.xtraTabPage7});
            this.xtraTabControl.UseDisabledStatePainter = false;
            this.xtraTabControl.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl_SelectedPageChanged);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.xtraTabPage1.Appearance.Header.BackColor = System.Drawing.Color.White;
            this.xtraTabPage1.Appearance.Header.BorderColor = System.Drawing.Color.White;
            this.xtraTabPage1.Appearance.Header.Image = global::miScout.Properties.Resources.new_game_icon;
            this.xtraTabPage1.Appearance.Header.Options.UseBackColor = true;
            this.xtraTabPage1.Appearance.Header.Options.UseBorderColor = true;
            this.xtraTabPage1.Appearance.Header.Options.UseImage = true;
            this.xtraTabPage1.Appearance.PageClient.BackColor = System.Drawing.Color.White;
            this.xtraTabPage1.Appearance.PageClient.BorderColor = System.Drawing.Color.White;
            this.xtraTabPage1.Appearance.PageClient.Options.UseBackColor = true;
            this.xtraTabPage1.Appearance.PageClient.Options.UseBorderColor = true;
            this.xtraTabPage1.Image = global::miScout.Properties.Resources.Item11;
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(959, 581);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Appearance.Header.BackColor = System.Drawing.Color.White;
            this.xtraTabPage2.Appearance.Header.BorderColor = System.Drawing.Color.White;
            this.xtraTabPage2.Appearance.Header.Options.UseBackColor = true;
            this.xtraTabPage2.Appearance.Header.Options.UseBorderColor = true;
            this.xtraTabPage2.Appearance.PageClient.BackColor = System.Drawing.Color.White;
            this.xtraTabPage2.Appearance.PageClient.Options.UseBackColor = true;
            this.xtraTabPage2.Image = global::miScout.Properties.Resources.Item2;
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(959, 581);
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.tableLayoutPanel2);
            this.xtraTabPage3.Controls.Add(this.tableLayoutPanel1);
            this.xtraTabPage3.Image = global::miScout.Properties.Resources.Picture3;
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(959, 581);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.pictureEdit3, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.countryBadge, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.pictureEdit1, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(690, 48);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(235, 393);
            this.tableLayoutPanel2.TabIndex = 30;
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureEdit3.BackgroundImage = global::miScout.Properties.Resources.Add_User_Male_100;
            this.pictureEdit3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.tableLayoutPanel2.SetColumnSpan(this.pictureEdit3, 2);
            this.pictureEdit3.EditValue = global::miScout.Properties.Resources.Player;
            this.pictureEdit3.Location = new System.Drawing.Point(30, 199);
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit3.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.StretchVertical;
            this.pictureEdit3.Size = new System.Drawing.Size(174, 191);
            this.pictureEdit3.TabIndex = 32;
            // 
            // countryBadge
            // 
            this.countryBadge.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.countryBadge.EditValue = global::miScout.Properties.Resources.Badge;
            this.countryBadge.Location = new System.Drawing.Point(120, 8);
            this.countryBadge.Name = "countryBadge";
            this.countryBadge.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.countryBadge.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.countryBadge.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.countryBadge.Size = new System.Drawing.Size(112, 180);
            this.countryBadge.TabIndex = 1;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureEdit1.EditValue = global::miScout.Properties.Resources.Badge;
            this.pictureEdit1.Location = new System.Drawing.Point(8, 50);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit1.Size = new System.Drawing.Size(100, 96);
            this.pictureEdit1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.textEdit7, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.labelControl7, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.btnSaveSettings, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.labelControl14, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.checkedComboBoxEdit3, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtPassword, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.labelControl8, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.labelControl13, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.checkedComboBoxEdit4, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtWeight, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.labelControl9, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.labelControl12, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.txtHeight, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.labelControl10, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.labelControl11, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.checkedComboBoxEdit6, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.checkedComboBoxEdit5, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.cmbPlayerLanguages, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.labelControl6, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.labelControl1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtFirstName, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelControl2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtLastName, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelControl3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelControl4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtPreferredName, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelControl5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.cmbNationality, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.dateOfBirth, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(28, 48);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 14;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(643, 445);
            this.tableLayoutPanel1.TabIndex = 29;
            // 
            // textEdit7
            // 
            this.textEdit7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textEdit7.Location = new System.Drawing.Point(135, 365);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.textEdit7.Size = new System.Drawing.Size(170, 20);
            this.textEdit7.TabIndex = 27;
            // 
            // labelControl7
            // 
            this.labelControl7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl7.Location = new System.Drawing.Point(3, 157);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(72, 16);
            this.labelControl7.TabIndex = 12;
            this.labelControl7.Text = "Current Club";
            // 
            // btnSaveSettings
            // 
            this.btnSaveSettings.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnSaveSettings.Location = new System.Drawing.Point(3, 404);
            this.btnSaveSettings.LookAndFeel.SkinName = "Visual Studio 2013 Dark";
            this.btnSaveSettings.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnSaveSettings.Name = "btnSaveSettings";
            this.btnSaveSettings.Size = new System.Drawing.Size(96, 26);
            this.btnSaveSettings.TabIndex = 28;
            this.btnSaveSettings.Text = "Save";
            this.btnSaveSettings.Click += new System.EventHandler(this.btnSaveSettings_Click);
            // 
            // labelControl14
            // 
            this.labelControl14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl14.Location = new System.Drawing.Point(3, 367);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(94, 16);
            this.labelControl14.TabIndex = 26;
            this.labelControl14.Text = "Verify Password";
            // 
            // checkedComboBoxEdit3
            // 
            this.checkedComboBoxEdit3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tableLayoutPanel1.SetColumnSpan(this.checkedComboBoxEdit3, 3);
            this.checkedComboBoxEdit3.EditValue = "";
            this.checkedComboBoxEdit3.Location = new System.Drawing.Point(135, 155);
            this.checkedComboBoxEdit3.Name = "checkedComboBoxEdit3";
            this.checkedComboBoxEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.checkedComboBoxEdit3.Properties.DataSource = this.clubsBindingSource;
            this.checkedComboBoxEdit3.Properties.DisplayMember = "Description";
            this.checkedComboBoxEdit3.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.checkedComboBoxEdit3.Properties.ValueMember = "Description";
            this.checkedComboBoxEdit3.Size = new System.Drawing.Size(335, 20);
            this.checkedComboBoxEdit3.TabIndex = 13;
            // 
            // clubsBindingSource
            // 
            this.clubsBindingSource.DataMember = "Clubs";
            this.clubsBindingSource.DataSource = this.miScoutDbDataSet;
            // 
            // miScoutDbDataSet
            // 
            this.miScoutDbDataSet.DataSetName = "miScoutDbDataSet";
            this.miScoutDbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // txtPassword
            // 
            this.txtPassword.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtPassword.Location = new System.Drawing.Point(135, 335);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtPassword.Properties.Appearance.Options.UseBackColor = true;
            this.txtPassword.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.txtPassword.Size = new System.Drawing.Size(170, 20);
            this.txtPassword.TabIndex = 25;
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl8.Location = new System.Drawing.Point(3, 187);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(94, 16);
            this.labelControl8.TabIndex = 14;
            this.labelControl8.Text = "Previous Club(s)";
            // 
            // labelControl13
            // 
            this.labelControl13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl13.Location = new System.Drawing.Point(3, 337);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(57, 16);
            this.labelControl13.TabIndex = 24;
            this.labelControl13.Text = "Password";
            // 
            // checkedComboBoxEdit4
            // 
            this.checkedComboBoxEdit4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tableLayoutPanel1.SetColumnSpan(this.checkedComboBoxEdit4, 3);
            this.checkedComboBoxEdit4.EditValue = "";
            this.checkedComboBoxEdit4.Location = new System.Drawing.Point(135, 185);
            this.checkedComboBoxEdit4.Name = "checkedComboBoxEdit4";
            this.checkedComboBoxEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.checkedComboBoxEdit4.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.checkedComboBoxEdit4.Size = new System.Drawing.Size(335, 20);
            this.checkedComboBoxEdit4.TabIndex = 15;
            // 
            // txtWeight
            // 
            this.txtWeight.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtWeight.Location = new System.Drawing.Point(135, 305);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtWeight.Properties.Appearance.Options.UseBackColor = true;
            this.txtWeight.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.txtWeight.Size = new System.Drawing.Size(99, 20);
            this.txtWeight.TabIndex = 23;
            // 
            // labelControl9
            // 
            this.labelControl9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl9.Location = new System.Drawing.Point(3, 217);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(47, 16);
            this.labelControl9.TabIndex = 16;
            this.labelControl9.Text = "Position";
            // 
            // labelControl12
            // 
            this.labelControl12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl12.Location = new System.Drawing.Point(3, 307);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(67, 16);
            this.labelControl12.TabIndex = 22;
            this.labelControl12.Text = "Weight (kg)";
            // 
            // txtHeight
            // 
            this.txtHeight.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtHeight.Location = new System.Drawing.Point(135, 275);
            this.txtHeight.Name = "txtHeight";
            this.txtHeight.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtHeight.Properties.Appearance.Options.UseBackColor = true;
            this.txtHeight.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.txtHeight.Size = new System.Drawing.Size(99, 20);
            this.txtHeight.TabIndex = 21;
            // 
            // labelControl10
            // 
            this.labelControl10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl10.Location = new System.Drawing.Point(3, 247);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(90, 16);
            this.labelControl10.TabIndex = 18;
            this.labelControl10.Text = "Other Positions";
            // 
            // labelControl11
            // 
            this.labelControl11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl11.Location = new System.Drawing.Point(3, 277);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(67, 16);
            this.labelControl11.TabIndex = 20;
            this.labelControl11.Text = "Height (cm)";
            // 
            // checkedComboBoxEdit6
            // 
            this.checkedComboBoxEdit6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tableLayoutPanel1.SetColumnSpan(this.checkedComboBoxEdit6, 3);
            this.checkedComboBoxEdit6.EditValue = "";
            this.checkedComboBoxEdit6.Location = new System.Drawing.Point(135, 245);
            this.checkedComboBoxEdit6.Name = "checkedComboBoxEdit6";
            this.checkedComboBoxEdit6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.checkedComboBoxEdit6.Properties.DataSource = this.playerDataBindingSource;
            this.checkedComboBoxEdit6.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.checkedComboBoxEdit6.Size = new System.Drawing.Size(335, 20);
            this.checkedComboBoxEdit6.TabIndex = 19;
            // 
            // playerDataBindingSource
            // 
            this.playerDataBindingSource.DataMember = "PlayerData";
            this.playerDataBindingSource.DataSource = this.miScoutDbDataSet;
            // 
            // checkedComboBoxEdit5
            // 
            this.checkedComboBoxEdit5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tableLayoutPanel1.SetColumnSpan(this.checkedComboBoxEdit5, 3);
            this.checkedComboBoxEdit5.EditValue = "";
            this.checkedComboBoxEdit5.Location = new System.Drawing.Point(135, 215);
            this.checkedComboBoxEdit5.Name = "checkedComboBoxEdit5";
            this.checkedComboBoxEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.checkedComboBoxEdit5.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.checkedComboBoxEdit5.Size = new System.Drawing.Size(335, 20);
            this.checkedComboBoxEdit5.TabIndex = 17;
            // 
            // cmbPlayerLanguages
            // 
            this.cmbPlayerLanguages.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tableLayoutPanel1.SetColumnSpan(this.cmbPlayerLanguages, 3);
            this.cmbPlayerLanguages.EditValue = "";
            this.cmbPlayerLanguages.Location = new System.Drawing.Point(135, 125);
            this.cmbPlayerLanguages.Name = "cmbPlayerLanguages";
            this.cmbPlayerLanguages.Properties.AllowMultiSelect = true;
            this.cmbPlayerLanguages.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPlayerLanguages.Properties.DataSource = this.languagesBindingSource;
            this.cmbPlayerLanguages.Properties.DisplayMember = "Description";
            this.cmbPlayerLanguages.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cmbPlayerLanguages.Properties.ValueMember = "Description";
            this.cmbPlayerLanguages.Size = new System.Drawing.Size(335, 20);
            this.cmbPlayerLanguages.TabIndex = 11;
            // 
            // languagesBindingSource
            // 
            this.languagesBindingSource.DataMember = "Languages";
            this.languagesBindingSource.DataSource = this.miScoutDbDataSet;
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl6.Location = new System.Drawing.Point(3, 127);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(111, 16);
            this.labelControl6.TabIndex = 10;
            this.labelControl6.Text = "Languages Spoken";
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl1.Location = new System.Drawing.Point(3, 7);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(64, 16);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "First Name";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtFirstName.Location = new System.Drawing.Point(135, 5);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtFirstName.Properties.Appearance.Options.UseBackColor = true;
            this.txtFirstName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.txtFirstName.Size = new System.Drawing.Size(209, 20);
            this.txtFirstName.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl2.Location = new System.Drawing.Point(350, 7);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(63, 16);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Last Name";
            // 
            // txtLastName
            // 
            this.txtLastName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtLastName.Location = new System.Drawing.Point(419, 5);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtLastName.Properties.Appearance.Options.UseBackColor = true;
            this.txtLastName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.txtLastName.Size = new System.Drawing.Size(221, 20);
            this.txtLastName.TabIndex = 3;
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl3.Location = new System.Drawing.Point(3, 37);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(72, 16);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Date of Birth";
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl4.Location = new System.Drawing.Point(3, 67);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(90, 16);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Preferred Name";
            // 
            // txtPreferredName
            // 
            this.txtPreferredName.Location = new System.Drawing.Point(135, 63);
            this.txtPreferredName.Name = "txtPreferredName";
            this.txtPreferredName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtPreferredName.Properties.Appearance.Options.UseBackColor = true;
            this.txtPreferredName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.txtPreferredName.Size = new System.Drawing.Size(209, 20);
            this.txtPreferredName.TabIndex = 7;
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl5.Location = new System.Drawing.Point(3, 97);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(61, 16);
            this.labelControl5.TabIndex = 8;
            this.labelControl5.Text = "Nationality";
            // 
            // cmbNationality
            // 
            this.cmbNationality.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cmbNationality.EditValue = "";
            this.cmbNationality.Location = new System.Drawing.Point(135, 95);
            this.cmbNationality.Name = "cmbNationality";
            this.cmbNationality.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbNationality.Properties.DataSource = this.countriesBindingSource;
            this.cmbNationality.Properties.DisplayMember = "Description";
            this.cmbNationality.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cmbNationality.Properties.ValueMember = "Description";
            this.cmbNationality.Size = new System.Drawing.Size(209, 20);
            this.cmbNationality.TabIndex = 9;
            this.cmbNationality.EditValueChanged += new System.EventHandler(this.cmbNationality_EditValueChanged);
            // 
            // countriesBindingSource
            // 
            this.countriesBindingSource.DataMember = "Countries";
            this.countriesBindingSource.DataSource = this.miScoutDbDataSet;
            // 
            // dateOfBirth
            // 
            this.dateOfBirth.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dateOfBirth.Location = new System.Drawing.Point(135, 34);
            this.dateOfBirth.Name = "dateOfBirth";
            this.dateOfBirth.Size = new System.Drawing.Size(209, 21);
            this.dateOfBirth.TabIndex = 5;
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Image = global::miScout.Properties.Resources.Item4;
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(959, 581);
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Image = global::miScout.Properties.Resources.Item5;
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(959, 581);
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Image = global::miScout.Properties.Resources.Item6;
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(959, 581);
            // 
            // xtraTabPage7
            // 
            this.xtraTabPage7.Image = global::miScout.Properties.Resources.Item7;
            this.xtraTabPage7.Name = "xtraTabPage7";
            this.xtraTabPage7.Size = new System.Drawing.Size(959, 581);
            // 
            // topPanel
            // 
            this.topPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.topPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.topPanel.Controls.Add(this.lblHeader);
            this.topPanel.Controls.Add(this.pictureBox1);
            this.topPanel.Location = new System.Drawing.Point(0, 31);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(1040, 70);
            this.topPanel.TabIndex = 1;
            // 
            // lblHeader
            // 
            this.lblHeader.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblHeader.Appearance.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeader.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblHeader.Location = new System.Drawing.Point(878, 25);
            this.lblHeader.LookAndFeel.SkinMaskColor = System.Drawing.Color.White;
            this.lblHeader.LookAndFeel.SkinName = "VS2010";
            this.lblHeader.LookAndFeel.UseDefaultLookAndFeel = false;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(0, 22);
            this.lblHeader.TabIndex = 9;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::miScout.Properties.Resources.Logo;
            this.pictureBox1.Location = new System.Drawing.Point(11, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(169, 50);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // CloseButton
            // 
            this.CloseButton.AllowFocus = false;
            this.CloseButton.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.CloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseButton.Appearance.BackColor = System.Drawing.Color.White;
            this.CloseButton.Appearance.BorderColor = System.Drawing.Color.White;
            this.CloseButton.Appearance.Options.UseBackColor = true;
            this.CloseButton.Appearance.Options.UseBorderColor = true;
            this.CloseButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.CloseButton.Image = ((System.Drawing.Image)(resources.GetObject("CloseButton.Image")));
            this.CloseButton.Location = new System.Drawing.Point(1004, 0);
            this.CloseButton.LookAndFeel.SkinMaskColor = System.Drawing.Color.White;
            this.CloseButton.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.White;
            this.CloseButton.LookAndFeel.SkinName = "Seven Classic";
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(36, 30);
            this.CloseButton.TabIndex = 2;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // languagesTableAdapter
            // 
            this.languagesTableAdapter.ClearBeforeFill = true;
            // 
            // clubsTableAdapter
            // 
            this.clubsTableAdapter.ClearBeforeFill = true;
            // 
            // countriesTableAdapter
            // 
            this.countriesTableAdapter.ClearBeforeFill = true;
            // 
            // playerDataTableAdapter1
            // 
            this.playerDataTableAdapter1.ClearBeforeFill = true;
            // 
            // ScoutForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1040, 690);
            this.ControlBox = false;
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.xtraTabControl);
            this.Controls.Add(this.topPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "ScoutForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.ScoutForm_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ScoutForm_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).EndInit();
            this.xtraTabControl.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.countryBadge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clubsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.miScoutDbDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerDataBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPlayerLanguages.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.languagesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPreferredName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbNationality.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.countriesBindingSource)).EndInit();
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage7;
        private DevExpress.XtraEditors.SimpleButton CloseButton;
        private DevExpress.XtraEditors.TextEdit txtFirstName;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtLastName;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private System.Windows.Forms.DateTimePicker dateOfBirth;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtPreferredName;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cmbNationality;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cmbPlayerLanguages;
        private DevExpress.XtraEditors.SimpleButton btnSaveSettings;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txtPassword;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit txtWeight;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txtHeight;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.CheckedComboBoxEdit checkedComboBoxEdit6;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.CheckedComboBoxEdit checkedComboBoxEdit5;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.CheckedComboBoxEdit checkedComboBoxEdit4;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.CheckedComboBoxEdit checkedComboBoxEdit3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.PictureEdit countryBadge;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl lblHeader;
        private miScoutDbDataSet miScoutDbDataSet;
        private System.Windows.Forms.BindingSource languagesBindingSource;
        private miScoutDbDataSetTableAdapters.LanguagesTableAdapter languagesTableAdapter;
        private System.Windows.Forms.BindingSource clubsBindingSource;
        private miScoutDbDataSetTableAdapters.ClubsTableAdapter clubsTableAdapter;
        private System.Windows.Forms.BindingSource countriesBindingSource;
        private miScoutDbDataSetTableAdapters.CountriesTableAdapter countriesTableAdapter;
        private System.Windows.Forms.BindingSource playerDataBindingSource;
        private miScoutDbDataSetTableAdapters.PlayerDataTableAdapter playerDataTableAdapter1;

    }
}

