FA Premier League
FA Premier League U-19
FA Premier Reserve League
FA Premier Academy League
League Championship
League Championship Play Offs
League One
League One Play Offs
League Two
League Two Play Offs
The Central League
The Football Combination
FA Cup
Football League Cup
FA Trophy
Football League Trophy
Manchester Senior Cup
FA Youth Cup
Victory Shield
Community Shield
FIFA World Cup
FIFA World Cup Qualifier
FIFA Confederations Cup
FIFA U-20 World Cup
FIFA U-21 World Cup Qualifier
FIFA U-19 World Cup
FIFA U-17 World Cup
FIFA Womens World Cup
Olympic Football Tournament
Olympic Football Tournament Qualifier
Womens Olympic Football Tournament
British U-16 Victory Shield
Cyprus Womens Cup
Gulf Cup of Nations
UNAF U23 Tournament
UEFA European Championship
UEFA European Championship Qualifier
UEFA U-17 European Championship
UEFA U-19 European Championship
UEFA U-19 European Championship Qualifier
UEFA U-21 European Championship
UEFA U-21 European Championship Qualifier
UEFA European Cup Winners Cup
UEFA European U-17 Championship
UEFA European U-19 Championship
UEFA European U-21 Championship
UEFA European U-21 Championship Qualifier
UEFA Womens European Championship
UEFA Womens European Championship Qualifier
UEFA Womens European U-17 Championship
UEFA Womens European U-17 Championship Qualifier
UEFA Womens European U-19 Championship
UEFA Womens European U-21 Championship
UEFA Champions League
UEFA Womens Champions League
UEFA Champions League Qualifier
UEFA Cup
UEFA Super Cup
UEFA Europa League
UEFA Europa League Qualifier
UEFA Intertoto Cup
European Cup Winners Cup
European Cup
AFC Asian Cup
AFC Asian Cup Qualifier
AFC Champions League
CAF Africa Cup of Nations
CAF Africa Cup of Nations Qualifier
CAF African Champions League
CAF Confederations Cup
CONCACAF Gold Cup
CONCACAF Champions League
CONCACAF Champions League Qualifier
CONCACAF U-20 Womens Championship
CONMEBOL Copa America
CONMEBOL Copa Libertadores
UNCAF Copa Centro Americana
A League
Argentine Primera Division
Austrian Football Bundesliga
Azerbaijan Premier League
Belgian First Division
Campeonato Brasileiro Serie A
Campeonato Carioca
Canadian Championship
Chile Primera Division
Chinese Super League
Prva HNL
1. Gambrinus Liga
Danish Superliga
Ligue 1
Finnish First Division
Bundesliga
Super League Greece
Eredivisie
Hungarian National Championship
Icelandic Urvalsdeild
Iran Pro League
Ligat HaAl
Serie A
J-League
K-League
Mexico Primera Division
Morocco Premier League
IFA Premiership
Tippeligaen
Ekstraklasa
Portuguese Liga
Qatar Stars League
Romanian Liga I
Russian First Division
Russian Premier League
Scottish Premier League
Scottish Third Division
S.League
National First Division
South African Premier League
Primera Division
Allsvenskan
Swiss Super League
Ukrainian Premier League
UAE Football League
UAE Reserve League
Major League Soccer
Major League Soccer Cup
Major League Soccer Play Offs
USL First Division
USL Second Division
USSF Division 2
Algarve Cup
Argentine Cup
Gulf Clubs Championship
Azerbaijan Cup
Brazilian Cup
Copa do Brasil
Canadian Championship
Coupe de la Ligue
German Cup
Greek Cup
Federation Cup
Federation Cup Qualifier
Icelandic Cup
Iranian Cup
Italian Cup
Kuwait Cup
Portuguese Cup
Qatar Cup
Russian Cup
Saudi Arabian Cup
Scottish FA Cup
Spanish Cup
UAE Cup
UAE League Golden Four Competition
UAE Super Cup
MLS Cup
Club Friendly
Womens In Region
International Friendly
International U-17 Friendly
International U-21 Friendly
NextGen Series
Reserve Friendly
BUCS Cup
PRO FC Academy
Big East Conference
Exhibition Match
Southeastern Conference
US Academy U-16
US Academy U-16 Showcase Play-offs
US Academy U-18
US Academy U-18 Showcase Play-offs
US College Soccer
US University In Conference
US University Out of Conference
USL Olympic Development Programme